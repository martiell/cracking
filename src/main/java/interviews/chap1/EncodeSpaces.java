package interviews.chap1;

public abstract class EncodeSpaces {
    abstract String encode(String str);

    final char[] toCharArray(String str) {
        char[] result = new char[str.length() + countSpaces(str) * 2];
        System.arraycopy(str.toCharArray(), 0, result, 0, str.length());
        return result;
    }

    private int countSpaces(String str) {
        int count = 0;
        for (int i = -1; (i = str.indexOf(' ', i + 1)) != -1; count++);
        return count;
    }

}
class EncodeSpaces1 extends EncodeSpaces {
    @Override
    public String encode(String str) {
        char[] chr = toCharArray(str);
        for (int i = 0; i < chr.length; i++) {
            if (chr[i] == ' ') {
                System.arraycopy(chr, i + 1, chr, i + 3, chr.length - i - 3);
                chr[i++] = '%';
                chr[i++] = '2';
                chr[i++] = '0';
            }
        }
        return new String(chr);
    }
}
class EncodeSpaces2 extends EncodeSpaces {
    @Override
    public String encode(String str) {
        char[] chr = toCharArray(str);
        int src = str.length() - 1;
        int dst = chr.length - 1;
        while (src >= 0) {
            if (chr[src] == ' ') {
                chr[dst--] = '0';
                chr[dst--] = '2';
                chr[dst--] = '%';
            } else {
                chr[dst--] = chr[src];
            }
            src--;
        }
        return new String(chr);
    }
}
