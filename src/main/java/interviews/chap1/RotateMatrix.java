package interviews.chap1;

public class RotateMatrix {

    static void rotate(int[][] matrix) {
        if (!checkMatrix(matrix)) {
            throw new IllegalArgumentException("Not a square matrix");
        }
        int l = matrix.length - 1;
        int[] tmp = new int[4];
        for (int i = 0; i < (matrix.length + 1) / 2; i++) {
            for (int j = 0; j < matrix.length / 2; j++) {
                // Using a tmp array, so that rotation can be modified using
                // index offset modulo length.
                tmp[0] = matrix[  j  ][  i  ];
                tmp[1] = matrix[  i  ][l - j];
                tmp[2] = matrix[l - j][l - i];
                tmp[3] = matrix[l - i][  j  ];
                matrix[  j  ][  i  ] = tmp[3];
                matrix[  i  ][l - j] = tmp[0];
                matrix[l - j][l - i] = tmp[1];
                matrix[l - i][  j  ] = tmp[2];
/*
                // Without the tmp array.
                int t = matrix[l - i][  j  ];
                matrix[l - i][  j  ] = matrix[l - j][l - i];
                matrix[l - j][l - i] = matrix[  i  ][l - j];
                matrix[  i  ][l - j] = matrix[  j  ][  i  ];
                matrix[  j  ][  i  ] = t;
*/
            }
        }
    }

    static boolean checkMatrix(int[][] matrix) {
        int l = matrix.length;
        for (int i = 0; i < l; i++) {
            if (matrix[i] == null || matrix[i].length != l) {
                return false;
            }
        }
        return true;
    }

}
