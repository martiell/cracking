package interviews.chap1;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Set;
import java.util.TreeSet;

public interface UniqueCharacters {
    boolean unique(String str);
}

class UniqueChars1 implements UniqueCharacters {
    @Override
    public boolean unique(String str) {
        if (str.length() < 2) {
            return true;
        }
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        int i = 1;
        while (i < chars.length && chars[i] != chars[i - 1]) {
            i++;
        }
        return i == chars.length;
    }
}

class UniqueChars2 implements UniqueCharacters {
    @Override
    public boolean unique(String str) {
        Set<Character> chars = new TreeSet<Character>();
        int i = 0;
        boolean duplicates = false;
        while (i < str.length() && !duplicates) {
            duplicates = chars.contains(str.charAt(i));
            chars.add(str.charAt(i));
            i++;
        }
        return !duplicates;
    }
}

class UniqueChars3 implements UniqueCharacters {
    @Override
    public boolean unique(String str) {
        BitSet flags = new BitSet(Character.MAX_VALUE);
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (flags.get(c)) {
                return false;
            }
            flags.set(c);
        }
        return true;
    }    
}
