package interviews.chap1;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public interface Permutation {
    boolean permuatation(String str1, String str2);
}

class PermutationSort implements Permutation {
    @Override
    public boolean permuatation(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        if (str1.length() < 2) {
            return true;
        }
        return sort(str1).equals(sort(str2));
    }

    private String sort(String str) {
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}

class PermutationMap implements Permutation {
    @Override
    public boolean permuatation(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        if (str1.length() < 2) {
            return true;
        }
        return countCharacters(str1).equals(countCharacters(str2));
    }

    private Map<Character, Integer> countCharacters(String str) {
        Map<Character, Integer> counts = new TreeMap<Character, Integer>();
        for (int i = 0; i < str.length(); i++) {
            Character chr = Character.valueOf(str.charAt(i));
            Integer count = counts.get(chr);
            counts.put(chr, count == null ? 0 : count + 1);
        }
        return counts;
    }

}
