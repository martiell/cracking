package interviews.chap1;

public class StringReverse {

    static void reverse(char[] str) {
        assert str != null;
        for (int i = 0, j = str.length - 1; i < j; i++, j--) {
            char tmp = str[i];
            str[i] = str[j];
            str[j] = tmp;
        }
    }

}
