package interviews.chap1;

public class RunLengthEncode {

    public static String maybeEncode(String str) {
        assert str != null;
        if (str.length() <= 2) {
            return str;
        }
        String encoded = runLengthEncode(str);
        return str.length() <= encoded.length() ? str : encoded;
    }

    private static String runLengthEncode(String input) {
        StringBuilder str = new StringBuilder();
        int count = 1;
        char last = input.charAt(0);
        int i = 1;
        while (i < input.length()) {
            char current = input.charAt(i++);
            if (last == current) {
                count++;
            } else {
                str.append(last);
                str.append(count);
                count = 1;
            }
            last = current;
        }
        str.append(last);
        str.append(count);
        return str.toString();
    }

}
