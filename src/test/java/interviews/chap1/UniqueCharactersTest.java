package interviews.chap1;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

@Test
public class UniqueCharactersTest {

    private final UniqueCharacters impl;

    public UniqueCharactersTest(UniqueCharacters impl) {
        this.impl = impl;
    }

    @Test(dataProvider = "examples")
    public void checkUnique(String str, boolean expected) {
        assertEquals(impl.unique(str), expected, impl.getClass().getSimpleName());
    }

    @DataProvider(name = "examples")
    public static Object[][] instancesToTest() {
        Object[][] matrix = new Object[][] {
                {"", true},
                {"a", true},
                {"ab", true},
                {"aa", false},
                {"aba", false},
                {"abc", true},
        };
        return matrix;
    }

    @Factory(dataProvider = "impls")
    public static Object[] factory(UniqueCharacters impl) {
        return new Object[] {new UniqueCharactersTest(impl)};
    }

    @DataProvider(name = "impls")
    public static Object[][] impls() {
        Object[][] impls = new Object[][] {
                {new UniqueChars1()},
                {new UniqueChars2()},
                {new UniqueChars3()},
        };
        return impls;
    }

}
