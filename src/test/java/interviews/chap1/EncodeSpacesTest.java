package interviews.chap1;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class EncodeSpacesTest {

    private final EncodeSpaces impl;

    public EncodeSpacesTest(EncodeSpaces impl) {
        this.impl = impl;
    }

    @Test(dataProvider = "examples")
    public void checkEncode(String str, String expected) {
        assertEquals(impl.encode(str), expected, impl.getClass().getSimpleName());
    }

    @DataProvider(name = "examples")
    public static Object[][] instancesToTest() {
        Object[][] matrix = new Object[][] {
                {"",      ""},
                {" ",     "%20"},
                {"a b",   "a%20b"},
                {"a",     "a"},
                {"aa",    "aa"},
                {"a b c", "a%20b%20c"},
        };
        return matrix;
    }

    @Factory(dataProvider = "impls")
    public static Object[] factory(EncodeSpaces impl) {
        return new Object[] {new EncodeSpacesTest(impl)};
    }

    @DataProvider(name = "impls")
    public static Object[][] impls() {
        Object[][] impls = new Object[][] {
                {new EncodeSpaces1()},
                {new EncodeSpaces2()},
        };
        return impls;
    }

}
