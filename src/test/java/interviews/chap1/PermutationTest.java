package interviews.chap1;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class PermutationTest {

    private final Permutation impl;

    public PermutationTest(Permutation impl) {
        this.impl = impl;
    }

    @Test(dataProvider = "examples")
    public void permutation(String str1, String str2, boolean expected) {
        assertEquals(impl.permuatation(str1, str2), expected,
                impl.getClass().getSimpleName());
    }

    @DataProvider(name = "examples")
    public Object[][] examples() {
        return new Object[][] {
                {"", "", true},
                {"a", "a", true},
                {"ab", "ba", true},
                {"ab", "bc", false},
        };
    }

    @Factory(dataProvider = "impls")
    public static Object[] factory(Permutation impl) {
        return new Object[] {new PermutationTest(impl)};
    }

    @DataProvider(name = "impls")
    public static Object[][] impls() {
        return new Object[][] {
                {new PermutationSort()},
                {new PermutationMap()},
        };
    }
}
