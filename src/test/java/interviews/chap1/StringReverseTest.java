package interviews.chap1;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class StringReverseTest {

    @Test(dataProvider = "examples")
    public void reverse(String input, String expected) {
        char[] chars = input.toCharArray();
        StringReverse.reverse(chars);
        assertEquals(new String(chars), expected);
    }

    @DataProvider(name = "examples")
    public Object[][] data() {
        return new Object[][] {
                {"", ""},
                {"a", "a"},
                {"ab", "ba"},
                {"abc", "cba"}
        };
    }

}
