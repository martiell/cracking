package interviews.chap1;

import static org.testng.Assert.assertTrue;

import java.util.Arrays;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class RotateMatrixTest {

    @Test(dataProvider = "examples")
    public void rotate(int[][] input, int[][] expected) {
        RotateMatrix.rotate(input);
        System.out.println(print(input));
        assertTrue(Arrays.deepEquals(input, expected));
    }

    private String print(int[][] matrix) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            str.append(Arrays.toString(matrix[i])).append('\n');
        }
        return str.toString();
    }

    @DataProvider(name = "examples")
    private Object[][] examples() {
        int[][] m1in = new int[][] {
                { 1, 2, 3},
                { 4, 5, 6},
                { 7, 8, 9},
        };
        int[][] m1out = new int[][] {
                { 7, 4, 1},
                { 8, 5, 2},
                { 9, 6, 3},
        };
        int[][] m2in = new int[][] {
                { 1,  2,  3,  4},
                { 5,  6,  7,  8},
                { 9, 10, 11, 12},
                {13, 14, 15, 16},
        };
        int[][] m2out = new int[][] {
                { 13,  9, 5, 1},
                { 14, 10, 6, 2},
                { 15, 11, 7, 3},
                { 16, 12, 8, 4},
        };
        return new Object[][] {
                {m1in, m1out},
                {m2in, m2out},
        };
    }
}
