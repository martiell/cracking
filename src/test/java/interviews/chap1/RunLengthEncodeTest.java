package interviews.chap1;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class RunLengthEncodeTest {

    @Test(dataProvider = "examples")
    public void encode(String input, String output) {
        assertEquals(RunLengthEncode.maybeEncode(input), output);
    }

    @DataProvider(name = "examples")
    public Object[][] examples() {
        return new Object[][] {
                {"", ""},
                {"a", "a"},
                {"aa", "aa"},
                {"aaa", "a3"},
                {"aabb", "aabb"},
                {"aaabb", "a3b2"},
                {"aaabbaaa", "a3b2a3"},
        };
    }
}
